# Stress testing of BuildGrid

## How to run

Use docker-compose and optionally set the environment variables
to the config options you want to test.

The following invocation includes all the possible options, as an example,
all of which are optional other than the basic
`docker-compose -f my-docker-compose.yml up`

```bash
STRESS_TESTING_DISABLE_PLATFORM_PROPERTIES=yes \
STRESS_TESTING_BGD_CONFIG=bgd-inmem.yml \
STRESS_TESTING_CLIENT_REQUESTS=6 \
 docker-compose -f docker-compose-pges-recc-bbrht.yml up \
  --scale buildgrid=2 --scale bots=4 \
  --scale clients=5 --scale database=3 -d --build
```

## Pro Tip

The included shell wizard script can generate the invocation script for you.
Just run: `bash docker-compose-up-gen.sh`, verify the command is what you need
and execute it.

## Additional Options and Supported Features
* `STRESS_TESTING_DISABLE_PLATFORM_PROPERTIES` — when this environment variable is set,
 workers and clients will NOT use any platform properties. The default is to generate
 a few random OS/ISA combinations (see `mnt/scripts` for more info).
* `STRESS_TESTING_BGD_CONFIG` — the BuildGrid config file to use from `mnt/scripts`.
* `STRESS_TESTING_CLIENT_REQUESTS` — the number of sequential requests the recc client
 will do.
* `STRESS_TESTING_DOCKER_COMPOSE` — The docker compose script to use when invoking any
 of the included utility shell scripts. Defaults to `docker-compose-pges-recc-bbrht.yml`.


### Checking status
```bash
export STDC=docker-compose-pges-recc.bbrht.yml

# To see the status of all the containers
docker-compose -f $STDC ps

# To see the status of all the containers that have NOT exited 0
docker-compose -f $STDC ps | grep -v "Exit 0"
# ... Or watch using the provided script
watch -n 0.5 -d scripts/stdc-ps-non0.sh
```
