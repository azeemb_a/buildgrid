#!/bin/bash

# Requires:
# -dialog
# -grep
# -ls
# -sed
# -tr

config_files="$(cd mnt/configs/ && ls -1 *.yml)"
config_options=$(grep -n "" <<< "$config_files" | sed 's/:/ /')

docker_compose_image="${STRESS_TESTING_DOCKER_COMPOSE:-docker-compose-pges-recc-bbrht.yml}"

dialog_config=$(dialog \
            --menu "Choose BuildGrid Config to use\n(from mnt/configs):" 15 40 10 $config_options \
            --form "Number of instances (--scale)" 15 40 0 \
            "BuildGrid:" 1 1 "1"   1 15 15 0 \
            "Postgres:"  2 1 "0"   2 15 15 0 \
            "Bots:"      3 1 "2"   3 15 15 0 \
            "Clients:"   4 1 "2"   4 15 15 0 \
            --form "Number of requests per client" 10 40 0 \
            "Client Requests:" 1 1 "1" 1 15 15 0 \
            --checklist "Additional Options" 15 50 5 \
            "NO_PP" "Disable Platform Properties" "off" \
            "DETACH" "Detach docker-compose (-d)" "off" \
            "BUILD" "Build images (--build)" "off" \
            --stdout)
dialog_rc=$?
clear
if [[ $dialog_rc -ne 0 ]]; then
   exit 1
fi

OIFS=$IFS; IFS=","
config=($(tr "[:space:]+" "," <<< "$dialog_config"))
IFS=$OIFS

# Use the input values in the appropriate places
bgd_config_index=${config[0]}
bgd_config=$(sed "${bgd_config_index}q;d" <<< "$config_files")

scale_bgd="${config[1]:-1}"
scale_pges="${config[2]:-0}"
scale_bots="${config[3]:-1}"
scale_clients="${config[4]:-1}"
# 5 is when form changes...
client_requests="${config[6]:-1}"

cmd_split=$' \\\n'

full_command="STRESS_TESTING_BGD_CONFIG=$bgd_config$cmd_split"
full_command+="STRESS_TESTING_CLIENT_REQUESTS=$client_requests$cmd_split"

full_command+=" docker-compose -f $docker_compose_image up$cmd_split"
full_command+="  --scale buildgrid=$scale_bgd --scale bots=$scale_bots$cmd_split"
full_command+="  --scale clients=$scale_clients --scale database=$scale_pges"

print_only=no

for opt in "${config[@]}"; do
    if [[ "$opt" == "NO_PP" ]]; then
        full_command="STRESS_TESTING_DISABLE_PLATFORM_PROPERTIES=yes$cmd_split$full_command"
    elif [[ "$opt" == "DETACH" ]]; then
        full_command+=" -d"
    elif [[ "$opt" == "BUILD" ]]; then
        full_command+=" --build"
    fi
done

echo "$full_command"

