
.. _configuration:

Configuration
=============

The details of how to tune BuildGrid's configuration.

.. hint::

   In order to spin-up a server instance using a given ``server.conf``
   configuration file, run:

   .. code-block:: sh

      bgd server start server.conf

   Please refer to the :ref:`CLI reference section <invoking-bgd-server>` for
   command line interface details.


.. _server-config-reference:

Reference configuration
-----------------------

Below is an example of the full configuration reference:

.. literalinclude:: ../../buildgrid/_app/settings/reference.yml
   :language: yaml

See the :ref:`Parser API reference <server-config-parser>` for details on the
tagged YAML nodes in this configuration.


.. _configuration-location:

Configuration location
----------------------

Unless a configuration file is explicitly specified on the command line when
invoking `bgd`, BuildGrid will always attempt to load configuration resources
from ``$XDG_CONFIG_HOME/buildgrid``. On most Linux based systems, the location
will be ``~/.config/buildgrid``.

This location is refered as ``$CONFIG_HOME`` is the rest of the document.


.. _tls-encryption:

TLS encryption
--------------

Every BuildGrid gRPC communication channel can be encrypted using SSL/TLS. By
default, the BuildGrid server will try to setup secure gRPC endpoints and return
in error if that fails. You must specify ``--allow-insecure`` explicitly if you
want it to use non-encrypted connections.

The TLS protocol handshake relies on an asymmetric cryptography system that
requires the server and the client to own a public/private key pair. BuildGrid
will try to load keys from these locations by default:

- Server private key: ``$CONFIG_HOME/server.key``
- Server public key/certificate: ``$CONFIG_HOME/server.crt``
- Client private key: ``$CONFIG_HOME/client.key``
- Client public key/certificate: ``$CONFIG_HOME/client.crt``


Server key pair
~~~~~~~~~~~~~~~

The TLS protocol requires a key pair to be used by the server. The following
example generates a self-signed key ``server.key``, which requires clients to
have a copy of the server certificate ``server.crt``. You can of course use a
key pair obtained from a trusted certificate authority instead.

.. code-block:: sh

   openssl req -new -newkey rsa:4096 -x509 -sha256 -days 3650 -nodes -batch -subj "/CN=localhost" -out server.crt -keyout server.key


Client key pair
~~~~~~~~~~~~~~~

If the server requires authentication in order to be granted special permissions
like uploading to CAS, a client side key pair is required. The following example
generates a self-signed key ``client.key``, which requires the server to have a
copy of the client certificate ``client.crt``.

.. code-block:: sh

   openssl req -new -newkey rsa:4096 -x509 -sha256 -days 3650 -nodes -batch -subj "/CN=client" -out client.crt -keyout client.key


.. _persisting-state:

Persisting Internal State
-------------------------

BuildGrid's Execution and Bots services can be configured to store their internal
state (such as the job queue) in an external data store of some kind. At the moment
the only supported type of data store is any SQL database with a driver supported
by SQLALchemy.

This makes it possible to restart a BuildGrid cluster without affecting the internal
state, alleviating concerns about having to finish currently queued or executing work
before restarting the scheduler or else accept losing track of that work. Upon
restarting, BuildGrid will load the jobs it previously knew about from the data
store, and recreate its internal state. Previous connections will need to be recreated
however, which can be done by a client sending a WaitExecution request with the
relevant operation name.


SQL Database
~~~~~~~~~~~~

The SQL data store implementation uses SQLAlchemy to connect to a database for storing
the job queue and related state.

There are database migrations provided, and BuildGrid can be configured to
automatically run them when connecting to the database. Alternatively, this can
be disabled and the migrations can be executed manually using Alembic.

When using the SQL Data Store with the default configuration (e.g. no `connection_string`),
a temporary SQLite database will be created for the lifetime of BuildGrid's execution.

.. hint::

  **SQLite in-memory databases are not supported** by BuildGrid to ensure multiple threads
  can share the same state database without any issues (using SQLAlchemy's `StaticPool`).

SQLite Configuration Block Example
**********************************

.. code-block:: yaml

  instances:
    - name: ''
  
      storages:
        - !lru-storage &cas-storage
          size: 2048M
  
      data-stores:
        - !sql-data-store &state-database
          storage: *cas-storage
          connection_string: sqlite:////path/to/sqlite.db
          # ... or don't specify the connection_string and BuildGrid will create a tempfile
  
      services:
        - !execution
          storage: *cas-storage
          data-store: *state-database


PostgreSQL Configuration Block Example
**************************************

.. code-block:: yaml

  instances:
    - name: ''
  
      storages:
        - !lru-storage &cas-storage
          size: 2048M
  
      data-stores:
        - !sql-data-store &state-database
          storage: *cas-storage
          connection_string: postgresql://username:password@sql_server/database_name
          # SQLAlchemy Pool Options
          pool_size: 5
          pool_timeout: 30
          max_overflow: 10

      services:
        - !execution
          storage: *cas-storage
          data-store: *state-database

 
With ``automigrate: no``, the migrations can be run by cloning the `git repository`_,
modifying the ``sqlalchemy.url`` line in ``alembic.ini`` to match the
``connection_string`` in the configuration, and executing

.. code-block:: sh

    tox -e venv -- alembic --config ./alembic.ini upgrade head

in the root directory of the repository. The docker-compose files in the
`git repository`_ offer an example approach for PostgreSQL.

.. hint::

   For the creation of the database and depending on the permissions and database config,
   you may need to create and initialize the database before Alembic can create all the
   tables for you.

   If Alembic fails to create the tables because it cannot read or create the ``alembic_version`` table,
   you could use the following SQL command:

   .. code-block:: sql

       CREATE TABLE alembic_version (
         version_num VARCHAR(32) NOT NULL,
         CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num))

.. _git repository: https://gitlab.com/BuildGrid/buildgrid


.. _monitoring-configuration:

Monitoring and Metrics
----------------------

BuildGrid provides a mechanism to output its logs in a number of formats, in addition
to printing them to stdout. Log messages can be formatted as JSON or the binary form
of the protobuf messages, and can be written to a file, a UNIX domain socket, or a
UDP port.

BuildGrid also provides some metrics to give insight into the current health and
utilisation of the BuildGrid instance. These metrics are protobuf messages similar to
the log messages, and can be configured in the same way. Additionally, metrics can be
formatted as statsd metrics strings, to allow simply configuring BuildGrid to output
its metrics to a remote StatsD server.

If the ``statsd`` format is used, then log messages are dropped and only metrics are
written to the configured endpoint. The log messages are still written to stdout in
this situation.


StatsD Metrics
~~~~~~~~~~~~~~

A common monitoring set up is to have metrics published into a StatsD server, for
aggregation and display using a tool like Grafana. BuildGrid's ``udp`` monitoring
``endpoint-type`` supports this trivially.

This configuration snippet will cause metrics to be published with a ``buildgrid``
prefix to a StatsD server listening on port 8125 with a hostname ``statsd-server``
which is resolvable by the BuildGrid instance.

.. code-block:: yaml

  monitoring:
    enabled: true
    endpoint-type: udp
    endpoint-location: statsd-server:8125
    serialization-format: statsd
    metric-prefix: buildgrid
