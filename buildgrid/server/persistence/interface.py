# Copyright (C) 2019 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABC, abstractmethod
import logging
from threading import Lock

from buildgrid._enums import JobEventType
from buildgrid.utils import JobWatchSpec


class DataStoreInterface(ABC):  # pragma: no cover

    """Abstract class defining an interface to a data storage backend.

    This provides methods for storing the internal state of BuildGrid,
    and retrieving it in order to reconstruct state on restart.

    """

    def __init__(self):
        self.logger = logging.getLogger(__file__)
        self.watched_jobs = {}
        self.watched_jobs_lock = Lock()

    # Monitoring configuration

    @abstractmethod
    def activate_monitoring(self):
        raise NotImplementedError()

    @abstractmethod
    def deactivate_monitoring(self):
        raise NotImplementedError()

    # Job API

    @abstractmethod
    def create_job(self, job):
        raise NotImplementedError()

    @abstractmethod
    def get_job_by_action(self, action_digest):
        raise NotImplementedError()

    @abstractmethod
    def get_job_by_name(self, name):
        raise NotImplementedError()

    @abstractmethod
    def get_job_by_operation(self, operation):
        raise NotImplementedError()

    @abstractmethod
    def get_all_jobs(self):
        raise NotImplementedError()

    @abstractmethod
    def get_jobs_by_stage(self, operation_stage):
        raise NotImplementedError()

    @abstractmethod
    def get_operations_by_stage(self, operation_stage):
        raise NotImplementedError()

    @abstractmethod
    def update_job(self, job_name, changes):
        raise NotImplementedError()

    @abstractmethod
    def delete_job(self, job_name):
        raise NotImplementedError()

    @abstractmethod
    def load_unfinished_jobs(self):
        raise NotImplementedError()

    def watch_job(self, job, operation_name, peer):
        """Start watching a job and operation.

        If the given job is already being watched, then this method finds (or adds)
        the operation in the job's entry in ``watched_jobs``, and adds the peer to
        the list of peers for that operation.

        Otherwise, it creates a whole new entry in ``watched_jobs`` for the given
        job, operation, and peer.

        This method runs in a thread spawned by gRPC handling a connected peer.

        Args:
            job (buildgrid.server.job.Job): The job to watch.
            operation_name (string): The name of the specific operation to
                watch.
            peer (string): The peer that is requesting to watch the job.

        """
        with self.watched_jobs_lock:
            spec = self.watched_jobs.get(job.name)
            if spec is None:
                self.watched_jobs[job.name] = spec = JobWatchSpec(job)
            spec.add_peer(operation_name, peer)
        self.logger.debug(
            "Registered peer [%s] to watch operation [%s] of job [%s]",
            peer, operation_name, job.name)

    def stream_operation_updates(self, operation_name, context):
        """Stream update messages for a given operation.

        This is a generator which yields tuples of the form

            (error, operation)

        where `error` is None unless the job is cancelled, in which case
        `error` is a `buildgrid._exceptions.CancelledError`.

        This method runs in a thread spawned by gRPC handling a connected
        peer, and should spend most of its time blocked waiting on an event
        which is set by either the thread which watches the data store for
        job updates or the main thread handling the gRPC termination
        callback.

        Iteration finishes either when the provided gRPC context becomes
        inactive, or when the job owning the operation being watched is
        deleted from the data store.

        Args:
            operation_name (string): The name of the operation to stream
                updates for.
            context (grpc.ServicerContext): The RPC context for the peer
                that is requesting a stream of events.

        """
        # Send an initial update as soon as we start watching, to provide the
        # peer with the initial state of the operation. This is done outside
        # the loop to simplify the logic for handling events without sending
        # unnecessary messages to peers.
        job = self.get_job_by_operation(operation_name)
        if job is None:
            return
        message = job.get_operation_update(operation_name)
        yield message

        self.logger.debug("Waiting for events")

        # Wait for events whilst the context is active. Events are set by the
        # thread which is watching the state data store for job updates.
        with self.watched_jobs_lock:
            event = self.watched_jobs[job.name].event
        last_event = None
        while context.is_active():
            last_event, event_type = event.wait(last_event)
            self.logger.debug("Received event #%s for operation [%s] with type [%s].",
                              last_event, operation_name, event_type)
            # A `JobEventType.STOP` event means that a peer watching this job
            # has disconnected and its termination callback has executed on
            # the thread gRPC creates for callbacks. In this case we don't
            # want to send a message, so we use `continue` to evaluate whether
            # or not to continue iteration.
            if event_type == JobEventType.STOP:
                continue

            job = self.get_job_by_operation(operation_name)
            if job is None:
                self.logger.debug(
                    "Job for operation [%s] has gone away, stopped streaming updates.",
                    operation_name)
                return
            message = job.get_operation_update(operation_name)
            yield message

        self.logger.debug("Context became inactive, stopped streaming updates.")

    def stop_watching_operation(self, job, operation_name, peer):
        """Remove the given peer from the list of peers watching the given job.

        If the given job is being watched, this method triggers a
        ``JobEventType.STOP`` for it to cause the waiting threads to check
        whether their context is still active. It then removes the given peer
        from the list of peers watching the given operation name. If this
        leaves no peers then the entire entry for the operation in the tracked
        job is removed.

        If this process leaves the job with no operations being watched, the
        job itself is removed from the `watched_jobs` dictionary, and it will
        no longer be checked for updates.

        This runs in the main thread as part of the RPC termination callback
        for ``Execute`` and ``WaitExecution`` requests.

        Args:
            job (buildgrid.server.job.Job): The job to stop watching.
            operation_name (string): The name of the specific operation to
                stop watching.
            peer (string): The peer that is requesting to stop watching the
                job.

        """
        with self.watched_jobs_lock:
            spec = self.watched_jobs.get(job.name)
            if spec is None:
                self.logger.debug(
                    "Peer [%s] attempted to stop watching job [%s] and operation "
                    "[%s], but no record of that job being watched was found.",
                    peer, job.name, operation_name)
                return
            spec.event.notify_stop()
            spec.remove_peer(operation_name, peer)
            if not spec.peers:
                self.logger.debug(
                    "No peers remain watching job [%s], removing it from the "
                    "dictionary of jobs being watched.", job.name)
                self.watched_jobs.pop(job.name)

    @abstractmethod
    def wait_for_job_updates(self, job, watch_spec):
        raise NotImplementedError()

    # Operation API

    @abstractmethod
    def create_operation(self, operation, job_name):
        raise NotImplementedError()

    @abstractmethod
    def get_operations_by_stage(self, operation_stage):
        raise NotImplementedError()

    @abstractmethod
    def get_all_operations(self):
        raise NotImplementedError()

    @abstractmethod
    def update_operation(self, operation_name, changes):
        raise NotImplementedError()

    @abstractmethod
    def delete_operation(self, operation_name):
        raise NotImplementedError()

    # Lease API

    @abstractmethod
    def create_lease(self, lease):
        raise NotImplementedError()

    @abstractmethod
    def get_leases_by_state(self, lease_state):
        raise NotImplementedError()

    @abstractmethod
    def update_lease(self, job_name, changes):
        raise NotImplementedError()

    @abstractmethod
    def assign_lease_for_next_job(self, capabilities, callback, timeout=None):
        raise NotImplementedError()
